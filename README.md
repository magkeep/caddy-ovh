# caddy-gandi
A simple Docker image with the OVH DNS provider module : [https://github.com/caddy-dns/ovh](https://github.com/caddy-dns/ovh).

Available at `registry.gitlab.com/magkeep/caddy-ovh`.